# SUITMEDIA CASE STUDY FRONTEND

## Feature

- Responsive
- Image Slider
- Mobile Hamburger Navigation Navigation
- Form Validation with react-hook-form and yup

## Tech Stack

- React 18
- Vite
- TypeScript
- Tailwind
- react-hook-form

## How To Run

1. Clone this project

```bash
git clone https://gitlab.com/bughowy/project-test-bughowy.git
```

2. Install all dependencies

```bash
yarn
```

3. Run project

```bash
yarn dev
```

## Deployment

This project is deployed on vercel.
[Visit](https://project-test-bughowy.vercel.app/)
