import { Navigation, Pagination } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";

export default function ImageSlider() {
  return (
    <>
      <Swiper
        modules={[Navigation, Pagination]}
        spaceBetween={0}
        navigation
        loop
        pagination={{ clickable: true }}
        onSlideChange={() => console.log("slide change")}
        onSwiper={(swiper) => console.log(swiper)}
      >
        <SwiperSlide className="relative">
          <div className="p-2 md:p-4 bg-black bg-opacity-50 absolute bottom-2 md:bottom-1/4 left-2 md:left-10 text-sm md:text-2xl text-white font-bold uppercase w-[90%] md:w-[50%]">
            We don't have the best but we have the greatest team
          </div>
          <img src="./slides/1.jpg" />
        </SwiperSlide>
        <SwiperSlide>
          <div className="p-2 md:p-4 bg-black bg-opacity-50 absolute bottom-2 md:bottom-1/4 left-2 md:left-10 text-sm md:text-2xl text-white font-bold uppercase w-[90%] md:w-[50%]">
            This is the place where technology & creativity fused into digital
            chemistry
          </div>
          <img src="./slides/2.jpg" />
        </SwiperSlide>
      </Swiper>
    </>
  );
}
